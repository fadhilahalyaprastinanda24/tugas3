package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView kumis, alis, janggut, rambut;
    CheckBox cKumis, cAlis, cJanggut, cRambut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kumis = findViewById(R.id.idKumis);
        alis = findViewById(R.id.idAlis);
        janggut = findViewById(R.id.idJanggut);
        rambut = findViewById(R.id.idRambut);

        cKumis = findViewById(R.id.boxKumis);
        cAlis = findViewById(R.id.boxAlis);
        cJanggut = findViewById(R.id.boxJanggut);
        cRambut = findViewById(R.id.bokRambut);

        kumis.setVisibility(View.INVISIBLE);
        alis.setVisibility(View.INVISIBLE);
        janggut.setVisibility(View.INVISIBLE);
        rambut.setVisibility(View.INVISIBLE);

        cKumis.setOnClickListener(this);
        cAlis.setOnClickListener(this);
        cJanggut.setOnClickListener(this);
        cRambut.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == cKumis.getId()) {
            if (kumis.getVisibility() == View.INVISIBLE) {
                kumis.setVisibility(View.VISIBLE);
            } else {
                kumis.setVisibility(View.INVISIBLE);
            }
        } else if (v.getId() == cAlis.getId()) {
            if (alis.getVisibility() == View.INVISIBLE) {
                alis.setVisibility(View.VISIBLE);
            } else {
                alis.setVisibility(View.INVISIBLE);
            }
        } else if (v.getId() == cJanggut.getId()) {
            if (janggut.getVisibility() == View.INVISIBLE) {
                janggut.setVisibility(View.VISIBLE);
            } else {
                janggut.setVisibility(View.INVISIBLE);
            }
        } else if (v.getId() == cRambut.getId()) {
            if (rambut.getVisibility() == View.INVISIBLE) {
                rambut.setVisibility(View.VISIBLE);
            } else {
                rambut.setVisibility(View.INVISIBLE);
            }
        }
    }
}
